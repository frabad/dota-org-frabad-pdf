﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:opentopic="http://www.idiominc.com/opentopic">

    <!-- Values are COLLAPSED or EXPANDED. If a value is passed in from Ant, use that value. -->
    <xsl:param name="bookmarkStyle">
      <xsl:choose>
        <xsl:when test="$antArgsBookmarkStyle!=''">
          <xsl:value-of select="$antArgsBookmarkStyle"/>
        </xsl:when>
        <xsl:otherwise>COLLAPSED</xsl:otherwise>
      </xsl:choose>
    </xsl:param>

    <!-- Determine how to style topics referenced by <chapter>, <part>, etc. Values are:
         MINITOC: render with a MiniToc on left, content indented on right.
         BASIC: render the same way as any topic. -->
    <xsl:param name="chapterLayout">
      <xsl:choose>
        <xsl:when test="$antArgsChapterLayout!=''">
          <xsl:value-of select="$antArgsChapterLayout"/>
        </xsl:when>
        <xsl:otherwise>MINITOC</xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:param name="appendixLayout" select="$chapterLayout"/>
    <xsl:param name="appendicesLayout" select="$chapterLayout"/>
    <xsl:param name="partLayout" select="$chapterLayout"/>
    <xsl:param name="noticesLayout" select="$chapterLayout"/>

    <!-- list of supported link roles -->
    <xsl:variable name="includeRelatedLinkRoles" select="tokenize(normalize-space($include.rellinks), '\s+')" as="xs:string*"/>

    <!-- Change page size to A4 -->
    <xsl:variable name="page-width">210mm</xsl:variable>
    <xsl:variable name="page-height">297mm</xsl:variable>
    
    <!-- This is the default, but you can set the margins individually below. -->
    <xsl:variable name="page-margins">20mm</xsl:variable>
    
    <!-- Change these if your page has different margins on different sides. -->
    <xsl:variable name="page-margin-inside">0.75in</xsl:variable>
    <xsl:variable name="page-margin-outside">1in</xsl:variable>
    <xsl:variable name="page-margin-top" select="$page-margins"/>
    <xsl:variable name="page-margin-bottom" select="$page-margins"/>

    <!-- The side column width is the amount the body text is indented relative to the margin. -->
    <xsl:variable name="side-col-width">1.25in</xsl:variable>

    <xsl:variable name="mirror-page-margins" select="true()"/>

    <xsl:variable name="default-font-size">10pt</xsl:variable>
    <xsl:variable name="default-line-height">12pt</xsl:variable>
    
    <!-- Static branding metadata -->
    <xsl:variable as="element()" name="theme">
      <xsl:variable as="element()" name="assets">
      <assets>
        <theme id="orange">
          <colors>
            <dark>#6c0006</dark>
            <medium>#f25925</medium>
            <light>#e57b00</light>
          </colors>
        </theme>
        <theme id="silver">
          <colors>
            <dark>#2f4f4f</dark>
            <medium>#708090</medium>
            <light>#778899</light>
          </colors>
        </theme>
        <theme id="black">
          <colors>
            <dark>black</dark>
            <medium>black</medium>
            <light>black</light>
          </colors>
        </theme>
        <product id="undefined" theme="black" />
        <product id="fruit" theme="orange" />
        <product id="metal" theme="silver" />
      </assets>
      </xsl:variable>
      <xsl:variable as="xs:string?" name="prodname"
        select="normalize-space(
          (/*/opentopic:map//*[contains(@class, ' topic/prodname ')])[1]
        )" />
      <xsl:variable as="node()" name="product" select="(
          $assets/product[@id=$prodname],
          $assets/product[@id='undefined']
        )[1]"
      />
      <xsl:sequence select="$assets/theme[@id=$product/@theme]" />
      <xsl:message select="concat(
        'Using [',$product/@theme,'] branding for [',$prodname,'] product')"
      />
    </xsl:variable>
    
</xsl:stylesheet>

