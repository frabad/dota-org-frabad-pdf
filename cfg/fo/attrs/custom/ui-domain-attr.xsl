<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

  <xsl:attribute-set name="_ui">
    <xsl:attribute name="font-family">Sans</xsl:attribute>
    <xsl:attribute name="font-size" select="concat($default-font-size,' * 0.9')" />
    <xsl:attribute name="line-height">100%</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="uicontrol" use-attribute-sets="_ui">
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="color">
      <xsl:choose>
        <xsl:when test="parent::*[contains(@class,'ui-d/menucascade')]">#000000</xsl:when>
        <xsl:otherwise>#808080</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="wintitle" use-attribute-sets="_ui">
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="color">#808080</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="menucascade" use-attribute-sets="_ui">
    <xsl:attribute name="color">#000000</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="shortcut">
    <xsl:attribute name="text-decoration">underline</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="screen" use-attribute-sets="base-font">
    <xsl:attribute name="space-before">1.2em</xsl:attribute>
    <xsl:attribute name="space-after">0.8em</xsl:attribute>
    <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
    <xsl:attribute name="white-space-collapse">false</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="background-color">#D0D0D0</xsl:attribute>
    <xsl:attribute name="font-family">monospace</xsl:attribute>
    <xsl:attribute name="line-height">100%</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="screen__top">
    <xsl:attribute name="leader-pattern">rule</xsl:attribute>
    <xsl:attribute name="leader-length">5.65in</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="screen__bottom">
      <xsl:attribute name="leader-pattern">rule</xsl:attribute>
      <xsl:attribute name="leader-length">5.65in</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
