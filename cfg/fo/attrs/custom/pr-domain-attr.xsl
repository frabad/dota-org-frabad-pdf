<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:attribute-set name="codeph" use-attribute-sets="base-font">
      <xsl:attribute name="font-family">monospace</xsl:attribute>
      <xsl:attribute name="font-size">0.8em</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="codeblock" use-attribute-sets="pre">
      <xsl:attribute name="keep-with-previous.within-page">always</xsl:attribute>
      <xsl:attribute name="start-indent">0.6em + from-parent(start-indent)</xsl:attribute>
      <xsl:attribute name="end-indent">0.6em + from-parent(end-indent)</xsl:attribute>
      <xsl:attribute name="padding">0.4em</xsl:attribute>
      <xsl:attribute name="background-color">#f0f0f0</xsl:attribute>
      <xsl:attribute name="border-style">solid</xsl:attribute>
      <xsl:attribute name="border-width">1px</xsl:attribute>
      <xsl:attribute name="border-color">silver</xsl:attribute>
      <xsl:attribute name="font-size">0.7em</xsl:attribute>
      <xsl:attribute name="line-height">1.4</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="codeblock__top">
      <xsl:attribute name="leader-pattern">rule</xsl:attribute>
      <xsl:attribute name="leader-length">100%</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="codeblock__bottom">
      <xsl:attribute name="leader-pattern">rule</xsl:attribute>
      <xsl:attribute name="leader-length">100%</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="option">
    </xsl:attribute-set>

    <xsl:attribute-set name="var">
      <xsl:attribute name="font-style">italic</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="parmname">
    </xsl:attribute-set>

    <xsl:attribute-set name="synph">
    </xsl:attribute-set>

    <xsl:attribute-set name="oper" use-attribute-sets="base-font">
      <xsl:attribute name="font-family">monospace</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="delim" use-attribute-sets="base-font">
      <xsl:attribute name="font-family">monospace</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="sep" use-attribute-sets="base-font">
      <xsl:attribute name="font-family">monospace</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="apiname" use-attribute-sets="base-font">
      <xsl:attribute name="font-family">monospace</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="parml">
    </xsl:attribute-set>

    <xsl:attribute-set name="plentry">
    </xsl:attribute-set>

    <xsl:attribute-set name="pt" use-attribute-sets="base-font">
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="text-indent">0em</xsl:attribute>
      <xsl:attribute name="end-indent">24pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="pt__content">
      <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="pd" use-attribute-sets="base-font">
      <xsl:attribute name="space-before">0.3em</xsl:attribute>
      <xsl:attribute name="space-after">0.5em</xsl:attribute>
      <xsl:attribute name="start-indent">72pt</xsl:attribute>
      <xsl:attribute name="end-indent">24pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="synblk">
    </xsl:attribute-set>

    <xsl:attribute-set name="synnoteref">
      <xsl:attribute name="baseline-shift">super</xsl:attribute>
      <xsl:attribute name="font-size">75%</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="synnote">
      <xsl:attribute name="baseline-shift">super</xsl:attribute>
      <xsl:attribute name="font-size">75%</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="syntaxdiagram">
    </xsl:attribute-set>

    <xsl:attribute-set name="fragment">
    </xsl:attribute-set>

    <xsl:attribute-set name="syntaxdiagram.title">
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="kwd" use-attribute-sets="base-font">
      <xsl:attribute name="font-family">monospace</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="kwd__default">
      <xsl:attribute name="text-decoration">underline</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="fragref" use-attribute-sets="base-font">
      <xsl:attribute name="font-family">monospace</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="fragment.title">
      <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="fragment.group">
    </xsl:attribute-set>

    <xsl:attribute-set name="syntaxdiagram.group">
    </xsl:attribute-set>

</xsl:stylesheet>
