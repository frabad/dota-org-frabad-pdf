<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    version="2.0">

    <xsl:attribute-set name="__frontmatter">
        <xsl:attribute name="text-align">end</xsl:attribute>
        <xsl:attribute name="font-family">Sans</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__productInfo">
        <xsl:attribute name="font-size">36pt</xsl:attribute>
        <xsl:attribute name="space-before">140mm</xsl:attribute>
        <xsl:attribute name="space-before.conditionality">retain</xsl:attribute>
        <xsl:attribute name="space-after">0</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="__frontmatter__productName">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="color" select="$theme/colors/medium" />
    </xsl:attribute-set>
    
    <xsl:attribute-set name="__frontmatter__productVersion">
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="color" select="$theme/colors/medium" />
    </xsl:attribute-set>
    
    <xsl:attribute-set name="__frontmatter__title__separator" use-attribute-sets="common.border__top">
        <xsl:attribute name="color" select="$theme/colors/light" />
        <xsl:attribute name="border-top-width">4pt</xsl:attribute>
        <xsl:attribute name="text-align">justify</xsl:attribute>
        <xsl:attribute name="text-align-last">end</xsl:attribute>
        <xsl:attribute name="start-indent">0mm</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="__frontmatter__title__separator__leader">
        <xsl:attribute name="leader-pattern">space</xsl:attribute>
        <xsl:attribute name="leader-length">100%</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="__frontmatter__title">
        <xsl:attribute name="font-family">Sans</xsl:attribute>
        <xsl:attribute name="space-before">0</xsl:attribute>
        <xsl:attribute name="font-size">24pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="color" select="$theme/colors/dark" />
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__subtitle">
        <xsl:attribute name="font-size">20pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">100%</xsl:attribute>
    </xsl:attribute-set>
  
    <!-- FIXME: is it used ? -->
    <xsl:attribute-set name="__frontmatter__owner">
        <xsl:attribute name="space-before">36pt</xsl:attribute>
        <xsl:attribute name="font-size">11pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
    </xsl:attribute-set>

    <!-- FIXME: is it used ? -->
    <xsl:attribute-set name="__frontmatter__owner__container">
        <xsl:attribute name="position">absolute</xsl:attribute>
        <xsl:attribute name="top">210mm</xsl:attribute>
        <xsl:attribute name="bottom">20mm</xsl:attribute>
        <xsl:attribute name="right">20mm</xsl:attribute>
        <xsl:attribute name="left">20mm</xsl:attribute>
    </xsl:attribute-set>

    <!-- FIXME: is it used ? -->
    <xsl:attribute-set name="__frontmatter__owner__container_content">
        <xsl:attribute name="text-align">center</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__mainbooktitle">
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__booklibrary">
    </xsl:attribute-set>
    
    <xsl:attribute-set name="__frontmatter__critdates">
      <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

  <xsl:attribute-set name="bookmap.summary">
    <xsl:attribute name="font-size">0.9em</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>

