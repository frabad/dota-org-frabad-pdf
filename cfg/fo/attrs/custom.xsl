<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">
                
  <xsl:include href="custom/basic-settings.xsl" />
  <xsl:include href="custom/front-matter-attr.xsl" />
  <xsl:include href="custom/static-content-attr.xsl" />
  <xsl:include href="custom/commons-attr.xsl" />
  <xsl:include href="custom/ui-domain-attr.xsl" />
  <xsl:include href="custom/lists-attr.xsl" />
  <xsl:include href="custom/pr-domain-attr.xsl" />
  <xsl:include href="custom/toc-attr.xsl" />
  <xsl:include href="custom/links-attr.xsl" />
  <xsl:include href="custom/task-elements-attr.xsl" />
  <xsl:include href="custom/tables-attr.xsl" />
  
</xsl:stylesheet>
