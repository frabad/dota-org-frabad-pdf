<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:dita2xslfo="http://dita-ot.sourceforge.net/ns/200910/dita2xslfo"
                xmlns:dita-ot="http://dita-ot.sourceforge.net/ns/201007/dita-ot"
                exclude-result-prefixes="dita-ot xs dita2xslfo"
>
  
  <xsl:include href="custom/root-processing.xsl" />
  <xsl:include href="custom/static-content.xsl" />
  <xsl:include href="custom/front-matter.xsl" />
  <xsl:include href="custom/commons.xsl" />
  <xsl:include href="custom/lists.xsl" />
  <xsl:include href="custom/tables.xsl" />
  <xsl:include href="custom/glossary.xsl" />
  
</xsl:stylesheet>
