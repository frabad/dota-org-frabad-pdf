<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:dita2xslfo="http://dita-ot.sourceforge.net/ns/200910/dita2xslfo"
                xmlns:dita-ot="http://dita-ot.sourceforge.net/ns/201007/dita-ot"
                exclude-result-prefixes="dita-ot xs dita2xslfo"
                xmlns:opentopic="http://www.idiominc.com/opentopic"
>
  
  <xsl:variable name="docTitle">
    <xsl:variable name="mainTitle">
      <xsl:choose>
        <xsl:when test="$map/*[contains(@class,' topic/title ')][1]">
          <xsl:apply-templates select="$map/*[contains(@class,' topic/title ')][1]"/>
        </xsl:when>
        <xsl:when test="$map//*[contains(@class,' bookmap/mainbooktitle ')][1]">
          <xsl:apply-templates select="$map//*[contains(@class,' bookmap/mainbooktitle ')][1]"/>
        </xsl:when>
        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
          <xsl:value-of select="//*[contains(@class, ' map/map ')]/@title"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mainTitle">
        <xsl:value-of select="$mainTitle"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="getVariable">
          <xsl:with-param name="id" select="'Document Title'"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="brandName">
    <xsl:variable name="mapBrandname" select="(/*/opentopic:map//*[contains(@class, ' topic/brand ')])[1]"/>
    <xsl:choose>
      <xsl:when test="$mapBrandname">
        <xsl:value-of select="$mapBrandname"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="getVariable">
          <xsl:with-param name="id" select="'Brand Name'"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="productName">
    <xsl:variable name="mapProdname" select="(/*/opentopic:map//*[contains(@class, ' topic/prodname ')])[1]"/>
    <xsl:choose>
      <xsl:when test="$mapProdname">
        <xsl:value-of select="$mapProdname"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="getVariable">
          <xsl:with-param name="id" select="'Product Name'"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="productVersion">
    <xsl:variable name="mapProdversion">
      <xsl:apply-templates select="(/*/opentopic:map//*[contains(@class,' topic/vrmlist ')])[1]" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mapProdversion">
        <xsl:value-of select="$mapProdversion"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="getVariable">
          <xsl:with-param name="id" select="'Product Version'"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="productInfo">
    <!-- TODO: call user variables -->
    <xsl:value-of select="$brandName,$productName,$productVersion" separator=" " />
  </xsl:variable>
    
  <xsl:variable name="copyrightHolder">
    <xsl:variable name="mapdata" select="(/*/opentopic:map//*[contains(@class, ' topic/copyrholder ')])[1]"/>
    <xsl:choose>
      <xsl:when test="$mapdata">
        <xsl:value-of select="$mapdata"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="getVariable">
          <xsl:with-param name="id" select="'Copyright Holder'"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="copyrightYear">
    <xsl:variable name="copyrtype" select="(/*/opentopic:map//*[contains(@class, ' topic/copyright ')])[1]/@type"/>
    <xsl:variable name="copyryears" select="distinct-values(/*/opentopic:map//*[contains(@class, ' topic/copyryear ')]/@year)"/>
    <xsl:choose>
      <xsl:when test="count($copyryears) &gt; 0">
        <xsl:value-of select="($copyrtype,'Copyright')[1]"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$copyryears" separator=", "/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="getVariable">
          <xsl:with-param name="id" select="'Copyright Year'"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <!-- various fixes for PDF meta processing -->
  
  <xsl:template match="/" mode="dita-ot:subject-metadata" as="xs:string?">
    <xsl:variable name="shortdesc">
      <xsl:for-each select="$map/*[contains(@class, ' bookmap/bookmeta ') or contains(@class, ' map/topicmeta ')][1]">
        <xsl:apply-templates select="*[contains(@class, ' map/shortdesc ')]" mode="dita-ot:text-only"/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="normalize-space($shortdesc) != ''">
        <xsl:value-of select="normalize-space($shortdesc)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="normalize-space($productInfo)" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="/" mode="dita-ot:keywords-metadata" as="xs:string*">
    <xsl:variable name="keywords" select="$map//
      *[contains(@class, ' topic/keywords ')]/
      *[contains(@class, ' topic/keyword ')]
    " />
    <xsl:for-each select="distinct-values($keywords)">
      <xsl:value-of select="."/>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="/" mode="dita-ot:author-metadata" as="xs:string?">
    <xsl:variable name="authorinformation" select="$map
      /*[contains(@class, ' map/topicmeta ')]
      /*[contains(@class, ' xnal-d/authorinformation ')]"
    />
    <xsl:variable name="mapauthors">
      <xsl:variable name="author" select="$map//*[contains(@class, ' topic/author ')]" />
      <xsl:for-each select="distinct-values($author)">
        <xsl:value-of select="." />
        <xsl:if test="position() != last()">, </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="exists($authorinformation/descendant::*[contains(@class, ' xnal-d/personname ')])">
        <xsl:for-each select="$authorinformation/descendant::*[contains(@class, ' xnal-d/personname ')][1]">
          <!-- Requires locale specific processing -->
          <xsl:value-of>
            <xsl:apply-templates select="*[contains(@class, ' xnal-d/firstname ')]/node()" mode="dita-ot:text-only"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="*[contains(@class, ' xnal-d/lastname ')]/node()" mode="dita-ot:text-only"/>
          </xsl:value-of>
        </xsl:for-each>
      </xsl:when>
      <xsl:when test="exists($authorinformation/descendant::*[contains(@class, ' xnal-d/organizationname ')])">
        <xsl:value-of>
          <xsl:apply-templates select="$authorinformation/descendant::*[contains(@class, ' xnal-d/organizationname ')]" mode="dita-ot:text-only"/>
        </xsl:value-of>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$mapauthors" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
