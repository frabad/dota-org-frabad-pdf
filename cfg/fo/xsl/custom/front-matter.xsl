<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function"
    xmlns:frabad="gitlab.com/frabad/dita-ot"
    version="2.0">

  <xsl:template name="createFrontMatter">
    <fo:page-sequence master-reference="front-matter"
      xsl:use-attribute-sets="__force__page__count">
      
      <xsl:call-template name="insertFrontMatterStaticContents"/>
      
      <fo:flow flow-name="xsl-region-body">
        
        <!-- set the fixed cover -->
        <xsl:call-template name="frabad:cover-image"/>
        
        <!-- front page info -->
        <fo:block xsl:use-attribute-sets="__frontmatter">
          
          <!-- display information about the product -->
          <fo:block xsl:use-attribute-sets="__frontmatter__productInfo">
            
            <!-- product name -->
            <fo:inline xsl:use-attribute-sets="__frontmatter__productName">
              <xsl:value-of select="$productName" />
            </fo:inline>
            <xsl:text> </xsl:text>
            <!-- product version -->
            <fo:inline xsl:use-attribute-sets="__frontmatter__productVersion">
              <xsl:value-of select="$productVersion" />
            </fo:inline>
            
          </fo:block>
          
          <!-- separator -->
          <fo:block xsl:use-attribute-sets="__frontmatter__title__separator">
            <fo:leader xsl:use-attribute-sets="__frontmatter__title__separator__leader" />
          </fo:block>
          
          <!-- main title -->
          <fo:block xsl:use-attribute-sets="__frontmatter__title">
            <xsl:value-of select="$docTitle" />
          </fo:block>
        
          <!-- critical dates -->
          <fo:block xsl:use-attribute-sets="__frontmatter__critdates">
            <xsl:apply-templates select="$map
              /*[contains(@class,' map/topicmeta ')]
              /*[contains(@class,' topic/critdates ')]"
            />
          </fo:block>
         
        </fo:block>

      </fo:flow>
    </fo:page-sequence>
    
  </xsl:template>
  
  <xsl:template name="frabad:cover-image">
    <xsl:variable name="image-path"
      select="($map//*[contains(@class, ' topic/data ')][@name = (
          'cover-image',
          'cover-image-path',
          'front-cover-image',
          'front-cover-image-path'
        )])[1]/@value" />
    <xsl:if test="$image-path">
      <xsl:variable name="href" select="
        if (opentopic-func:isAbsolute($image-path))
        then $image-path
        else concat($input.dir.url, $image-path)" />
      <fo:block-container absolute-position="fixed">
        <fo:block>
          <fo:external-graphic
            content-width="scale-to-fit"
            width="{$page-width}"
            src="url({$href})"
          />
        </fo:block>
      </fo:block-container>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>

