<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function"
  xmlns:frabad="gitlab.com/frabad/dita-ot"
>

  <xsl:template name="frabad:footer-image">
    <xsl:variable name="image-path"
      select="($map//*[contains(@class, ' topic/data ')][@name = (
          'footer-image',
          'footer-image-path'
        )])[1]/@value" />
    <xsl:if test="$image-path">
      <xsl:variable name="href" select="
        if (opentopic-func:isAbsolute($image-path))
        then $image-path
        else concat($input.dir.url, $image-path)" />
      <fo:external-graphic
        content-width="scale-to-fit"
        height="32px"
        src="url({$href})"
      />
    </xsl:if>
  </xsl:template>
  
  <!--FIXME: use the template model provided by DITA-OT 2.x-->
  
  <!-- heading attributes for odd pages -->
  <xsl:template name="__body__odd__header__heading__attr">
    <xsl:attribute name="text-align">end</xsl:attribute>
    <xsl:attribute name="end-indent">1em</xsl:attribute>
    <xsl:attribute name="border-end-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">medium</xsl:attribute>
    <xsl:attribute name="border-color">
      <xsl:value-of select="$theme/colors/dark" />
    </xsl:attribute>
  </xsl:template>
  
  <!-- heading attributes for even pages -->
  <xsl:template name="__body__even__header__heading__attr">
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="start-indent">1em</xsl:attribute>
    <xsl:attribute name="border-start-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">medium</xsl:attribute>
    <xsl:attribute name="border-color">
      <xsl:value-of select="$theme/colors/dark" />
    </xsl:attribute>
  </xsl:template>
  
  <!-- TODO: move footer image references to brand-data or vars/ -->
  
  <xsl:template name="insertHeaderTable">
    <xsl:param name="left" />
    <xsl:param name="right" />
    <xsl:param name="align" />
    <xsl:variable name="short-width">5em</xsl:variable>
    <fo:table table-layout="fixed" width="100%">
      <xsl:choose>
        <xsl:when test="$align='end' or $align='right'">
          <xsl:element name="fo:table-column" />
          <xsl:element name="fo:table-column">
            <xsl:attribute name="column-width">
              <xsl:value-of select="$short-width" />
            </xsl:attribute>
          </xsl:element>
        </xsl:when>
        <xsl:when test="$align='start' or $align='left'">
          <xsl:element name="fo:table-column">
            <xsl:attribute name="column-width">
              <xsl:value-of select="$short-width" />
            </xsl:attribute>
          </xsl:element>
          <xsl:element name="fo:table-column" />
        </xsl:when>
      </xsl:choose>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell display-align="center">
            <xsl:choose>
              <xsl:when test="$align='end' or $align='right'">
                <xsl:call-template name="__body__odd__header__heading__attr" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="text-align">center</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <fo:block>
              <xsl:copy-of select="$left" />
            </fo:block>
          </fo:table-cell>
          <fo:table-cell display-align="center">
            <xsl:choose>
              <xsl:when test="$align='start' or $align='left'">
                <xsl:call-template name="__body__even__header__heading__attr" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="text-align">center</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <fo:block>
              <xsl:copy-of select="$right" />
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>
  
  <xsl:template name="insertFooterTable">
    <xsl:param name="left" />
    <xsl:param name="right" />
    <fo:table xsl:use-attribute-sets="table.tgroup">
      <fo:table-column column-width="proportional-column-width(1)"/>
      <fo:table-column column-width="proportional-column-width(1)"/>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell text-align="start" display-align="center">
            <fo:block start-indent="0" end-indent="0">
              <xsl:copy-of select="$left" />
            </fo:block>
          </fo:table-cell>
          <fo:table-cell text-align="end" display-align="center">
            <fo:block start-indent="0" end-indent="0">
              <xsl:copy-of select="$right" />
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>
  
  
  <!-- preface pages -->
  
  <xsl:template name="insertPrefaceFirstHeader">
    <fo:static-content flow-name="first-body-header">
      <fo:block xsl:use-attribute-sets="__body__first__header">    
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">end</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface first header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__first__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface first header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__body__first__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>                
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertPrefaceFirstFooter">
    <fo:static-content flow-name="first-body-footer">
      <fo:block xsl:use-attribute-sets="__body__first__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface first footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__first__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__body__first__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertPrefaceOddHeader">
    <fo:static-content flow-name="odd-body-header">
      <fo:block xsl:use-attribute-sets="__body__odd__header">
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">end</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface odd header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__odd__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface odd header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__body__odd__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertPrefaceEvenHeader">
    <fo:static-content flow-name="even-body-header">
      <fo:block xsl:use-attribute-sets="__body__even__header">
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">start</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface even header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__body__even__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface even header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__even__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>
  
  <xsl:template name="insertPrefaceOddFooter">
    <fo:static-content flow-name="odd-body-footer">
      <fo:block xsl:use-attribute-sets="__body__odd__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface odd footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__odd__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__body__odd__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertPrefaceEvenFooter">
    <fo:static-content flow-name="even-body-footer">
      <fo:block xsl:use-attribute-sets="__body__even__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface even footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__even__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__body__even__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>
  
  
  <!-- body pages -->
  
  <xsl:template name="insertBodyFirstHeader">
    <fo:static-content flow-name="first-body-header">
      <fo:block xsl:use-attribute-sets="__body__first__header">    
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">end</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body first header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__first__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body first header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__body__first__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertBodyFirstFooter">
    <fo:static-content flow-name="first-body-footer">
      <fo:block xsl:use-attribute-sets="__body__first__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body first footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__first__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__body__first__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertBodyOddHeader">
    <fo:static-content flow-name="odd-body-header">
      <fo:block xsl:use-attribute-sets="__body__odd__header">
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">end</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body odd header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__odd__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body odd header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__body__odd__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertBodyEvenHeader">
    <fo:static-content flow-name="even-body-header">
      <fo:block xsl:use-attribute-sets="__body__even__header">
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">start</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body even header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__body__even__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body even header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__even__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>
  
  <xsl:template name="insertBodyOddFooter">
    <fo:static-content flow-name="odd-body-footer">
      <fo:block xsl:use-attribute-sets="__body__odd__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body odd footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__odd__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__body__odd__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertBodyEvenFooter">
    <fo:static-content flow-name="even-body-footer">
      <fo:block xsl:use-attribute-sets="__body__even__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Body even footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__even__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__body__even__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>
  
  
  <!-- TOC pages -->
  
  <xsl:template name="insertTocOddHeader">
    <fo:static-content flow-name="odd-toc-header">
      <fo:block xsl:use-attribute-sets="__toc__odd__header">
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">end</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Toc odd header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__odd__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Toc odd header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__toc__odd__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>
  
  <xsl:template name="insertTocEvenHeader">
    <fo:static-content flow-name="even-toc-header">
      <fo:block xsl:use-attribute-sets="__toc__even__header">
        <xsl:call-template name="insertHeaderTable">
          <xsl:with-param name="align">start</xsl:with-param>
          <xsl:with-param name="left">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Toc even header'"/>
              <xsl:with-param name="params">
                <pagenum>
                  <fo:block xsl:use-attribute-sets="__toc__even__header__pagenum">
                    <fo:page-number/>
                  </fo:block>
                </pagenum>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Toc even header'"/>
              <xsl:with-param name="params">
                <prodname>
                  <fo:block>
                    <xsl:value-of select="$productInfo"/>
                  </fo:block>
                </prodname>
                <heading>
                  <fo:block xsl:use-attribute-sets="__body__even__header__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:block>
                </heading>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertTocOddFooter">
    <fo:static-content flow-name="odd-toc-footer">
      <fo:block xsl:use-attribute-sets="__toc__odd__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Toc odd footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__odd__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__toc__odd__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>

  <xsl:template name="insertTocEvenFooter">
    <fo:static-content flow-name="even-toc-footer">
      <fo:block xsl:use-attribute-sets="__toc__even__footer">
        <xsl:call-template name="insertFooterTable">
          <xsl:with-param name="left">
            <xsl:call-template name="frabad:footer-image"/>
          </xsl:with-param>
          <xsl:with-param name="right">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Toc even footer'"/>
              <xsl:with-param name="params">
                <heading>
                  <fo:inline xsl:use-attribute-sets="__body__even__footer__heading">
                    <fo:retrieve-marker retrieve-class-name="current-header"/>
                  </fo:inline>
                </heading>
                <pagenum>
                  <fo:inline xsl:use-attribute-sets="__toc__even__footer__pagenum">
                    <fo:page-number/>
                  </fo:inline>
                </pagenum>
                <copyright>
                  <fo:inline>
                    <xsl:call-template name="getVariable">
                      <xsl:with-param name="id" select="'Copyright'"/>
                      <xsl:with-param name="params">
                        <copyryear>
                          <xsl:value-of select="$copyrightYear" />
                        </copyryear>
                        <copyrholder>
                          <xsl:value-of select="$copyrightHolder" />
                        </copyrholder>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:inline>
                </copyright>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </fo:block>
    </fo:static-content>
  </xsl:template>
  
</xsl:stylesheet>
