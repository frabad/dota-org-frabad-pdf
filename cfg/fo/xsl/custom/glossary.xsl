﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:ot-placeholder="http://suite-sol.com/namespaces/ot-placeholder"
                extension-element-prefixes="ot-placeholder"
                version="2.0">

  <xsl:template match="*[contains(@class, ' glossentry/glossBody ')]">
    <!--prevent useless body data from being displayed-->
  </xsl:template>

</xsl:stylesheet>
