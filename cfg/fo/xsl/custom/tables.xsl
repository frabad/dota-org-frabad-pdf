<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:dita2xslfo="http://dita-ot.sourceforge.net/ns/200910/dita2xslfo"
  xmlns:exsl="http://exslt.org/common"
  xmlns:exslf="http://exslt.org/functions"
  exclude-result-prefixes="exsl exslf opentopic-func xs dita2xslfo"
  version="2.0">

  <!--Definition list-->
  
  <xsl:template match="*[contains(@class, ' topic/dl ')]" mode="table">
  
    <!-- frabad: Set relative column widths from the 'relcolwidth'
                 processing-instruction or fall back to default value -->
    <xsl:param name="relcolwidth">    
      <xsl:variable name="pi" select="
        normalize-space(descendant::processing-instruction('relcolwidth')[1])
      " />
      <xsl:choose>
        <!-- only numbers wanted -->
        <xsl:when test="number(replace($pi,'\s+',''))">
          <xsl:value-of select="$pi" />
        </xsl:when>
        <!-- default proportional widths -->
        <xsl:otherwise>1 3</xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    
    <fo:table xsl:use-attribute-sets="dl">
      <xsl:call-template name="commonattributes" />
      
      <!-- frabad: add proportional-column-width attributes based on $relcolwidth values -->
      <xsl:for-each select="tokenize($relcolwidth,'\s')[position() le 2]">
        <xsl:element name="fo:table-column">
          <xsl:attribute name="column-width">
            <xsl:value-of select="concat('proportional-column-width(',.,')')" />
          </xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      
      <!-- frabad: only apply dlhead if not empty, to avoid further XSL-FO errors -->
      <xsl:apply-templates select="*[contains(@class, ' topic/dlhead ')][*]" />
      
      <fo:table-body xsl:use-attribute-sets="dl__body">
        <xsl:choose>
          <xsl:when test="contains(@otherprops,'sortable')">
            <xsl:apply-templates select="*[contains(@class, ' topic/dlentry ')]" mode="table">
              <xsl:sort select="opentopic-func:getSortString(normalize-space( opentopic-func:fetchValueableText(*[contains(@class, ' topic/dt ')]) ))" lang="{$locale}" />
            </xsl:apply-templates>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="*[contains(@class, ' topic/dlentry ')]" mode="table" />
          </xsl:otherwise>
        </xsl:choose>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template match="*[contains(@class, ' topic/dlentry ')]" mode="table">
    <fo:table-row xsl:use-attribute-sets="dlentry">
      <xsl:call-template name="commonattributes"/>
      <fo:table-cell xsl:use-attribute-sets="dlentry.dt">
        <xsl:apply-templates select="*[contains(@class, ' topic/dt ')]" />
      </fo:table-cell>
      <fo:table-cell xsl:use-attribute-sets="dlentry.dd">
        <xsl:apply-templates select="*[contains(@class, ' topic/dd ')]" />
      </fo:table-cell>
    </fo:table-row>
  </xsl:template>

</xsl:stylesheet>
