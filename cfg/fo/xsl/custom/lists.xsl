<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:opentopic-func="http://www.idiominc.com/opentopic/exsl/function"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:dita2xslfo="http://dita-ot.sourceforge.net/ns/200910/dita2xslfo"
  xmlns:exsl="http://exslt.org/common"
  xmlns:exslf="http://exslt.org/functions"
  exclude-result-prefixes="exsl exslf opentopic-func xs dita2xslfo"
  version="2.0">

  <!-- Definition list -->

  <xsl:template match="*[contains(@class, ' topic/dl ')]">
    <xsl:choose>
      <xsl:when test="child::*[contains(@class, ' topic/dlhead ')]">
        <xsl:apply-templates select="." mode="table" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="list" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*[contains(@class, ' topic/dl ')]" mode="list">
    <fo:block xsl:use-attribute-sets="dl__list">
      <xsl:call-template name="commonattributes" />
      <fo:block xsl:use-attribute-sets="dl__list__body">
        <xsl:choose>
          <xsl:when test="contains(@otherprops,'sortable')">
            <xsl:apply-templates select="*[contains(@class, ' topic/dlentry ')]" mode="list">
              <xsl:sort select="opentopic-func:getSortString(normalize-space( opentopic-func:fetchValueableText(*[contains(@class, ' topic/dt ')]) ))" lang="{$locale}" />
            </xsl:apply-templates>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="*[contains(@class, ' topic/dlentry ')]" mode="list" />
          </xsl:otherwise>
        </xsl:choose>
      </fo:block>
    </fo:block>
  </xsl:template>

  <xsl:template match="*[contains(@class, ' topic/dlentry ')]" mode="list">
    <fo:block xsl:use-attribute-sets="dlentry__list">
      <xsl:call-template name="commonattributes"/>
      <xsl:apply-templates select="*[contains(@class, ' topic/dt ')]" mode="list" />
      <xsl:apply-templates select="*[contains(@class, ' topic/dd ')]" mode="list" />
    </fo:block>
  </xsl:template>
  
  <xsl:template match="*[contains(@class, ' topic/dt ')]" mode="list">
    <fo:block xsl:use-attribute-sets="dlentry__list.dt">
      <xsl:call-template name="commonattributes"/>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match="*[contains(@class, ' topic/dd ')]" mode="list">
    <fo:block xsl:use-attribute-sets="dlentry__list.dd">
      <xsl:call-template name="commonattributes"/>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

</xsl:stylesheet>