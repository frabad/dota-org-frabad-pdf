<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:dita2xslfo="http://dita-ot.sourceforge.net/ns/200910/dita2xslfo"
                xmlns:dita-ot="http://dita-ot.sourceforge.net/ns/201007/dita-ot"
                exclude-result-prefixes="dita-ot xs dita2xslfo"
>

  <xsl:template match="*[contains(@class,' topic/critdates ')]">
    <xsl:variable name="revisions" as="xs:string*">
      <xsl:for-each select="*[contains(@class,' topic/revised ')][@modified castable as xs:date]">
        <xsl:sort select="@modified" order="ascending" />
        <xsl:value-of select="@modified" />
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="revisedPrefix">
      <xsl:call-template name="getVariable">
        <xsl:with-param name="id" select="'Revision Date'" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="count($revisions) gt 0">
      <xsl:value-of select="($revisedPrefix,
        ( if (count($revisions) gt 1)
          then ('',count($revisions),'&#x2013;',$revisions[last()])
          else (format-date(xs:date($revisions[last()]), '[Y].[d]'))
        ))" />
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="*[contains(@class,' topic/prodinfo ')]">
    <xsl:apply-templates select="*[contains(@class,' topic/brand ')][1]" />
    <xsl:apply-templates select="*[contains(@class,' topic/prodname ')][1]" />
    <xsl:apply-templates select="*[contains(@class,' topic/vrmlist ')][1]" />
  </xsl:template>
  
  <xsl:template match="*[contains(@class,' topic/vrmlist ')]">
    <xsl:apply-templates select="*[contains(@class,' topic/vrm ')][1]" />
  </xsl:template>
  
  <xsl:template match="*[contains(@class,' topic/vrm ')]">
    <xsl:variable name="prefix"> </xsl:variable>
    <xsl:if test="@version">
      <xsl:value-of select="$prefix" />
    </xsl:if>
    <xsl:value-of select="@version,@release,@modification" separator="." />
  </xsl:template>
  
  <!-- remove ugly linebreaks in Chapter with number
    = replace fo:block with fo:inline
  -->
  <xsl:template name="insertChapterFirstpageStaticContent">
    <xsl:param name="type"/>
    <fo:block>
      <xsl:attribute name="id">
        <xsl:call-template name="generate-toc-id"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="$type = 'chapter'">
          <fo:block xsl:use-attribute-sets="__chapter__frontmatter__name__container">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Chapter with number'"/>
              <xsl:with-param name="params">
                <number>
                  <fo:inline xsl:use-attribute-sets="__chapter__frontmatter__number__container">
                    <xsl:apply-templates select="key('map-id', @id)[1]" mode="topicTitleNumber"/>
                  </fo:inline>
                </number>
              </xsl:with-param>
            </xsl:call-template>
          </fo:block>
        </xsl:when>
        <xsl:when test="$type = 'appendix'">
          <fo:block xsl:use-attribute-sets="__chapter__frontmatter__name__container">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Appendix with number'"/>
              <xsl:with-param name="params">
                <number>
                  <fo:inline xsl:use-attribute-sets="__chapter__frontmatter__number__container">
                    <xsl:apply-templates select="key('map-id', @id)[1]" mode="topicTitleNumber"/>
                  </fo:inline>
                </number>
              </xsl:with-param>
            </xsl:call-template>
          </fo:block>
        </xsl:when>
        <xsl:when test="$type = 'appendices'">
          <fo:block xsl:use-attribute-sets="__chapter__frontmatter__name__container">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Appendix with number'"/>
              <xsl:with-param name="params">
                <number>
                  <fo:inline xsl:use-attribute-sets="__chapter__frontmatter__number__container">
                    <xsl:text>&#xA0;</xsl:text>
                  </fo:inline>
                </number>
              </xsl:with-param>
            </xsl:call-template>
          </fo:block>
        </xsl:when>
        <xsl:when test="$type = 'part'">
          <fo:block xsl:use-attribute-sets="__chapter__frontmatter__name__container">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Part with number'"/>
              <xsl:with-param name="params">
                <number>
                  <fo:inline xsl:use-attribute-sets="__chapter__frontmatter__number__container">
                    <xsl:apply-templates select="key('map-id', @id)[1]" mode="topicTitleNumber"/>
                  </fo:inline>
                </number>
              </xsl:with-param>
            </xsl:call-template>
          </fo:block>
        </xsl:when>
        <xsl:when test="$type = 'preface'">
          <fo:block xsl:use-attribute-sets="__chapter__frontmatter__name__container">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Preface title'"/>
            </xsl:call-template>
          </fo:block>
        </xsl:when>
        <xsl:when test="$type = 'notices'">
          <fo:block xsl:use-attribute-sets="__chapter__frontmatter__name__container">
            <xsl:call-template name="getVariable">
              <xsl:with-param name="id" select="'Notices title'"/>
            </xsl:call-template>
          </fo:block>
        </xsl:when>
      </xsl:choose>
    </fo:block>
  </xsl:template>
  
  <!-- move figure title to top and description to bottom -->
  <xsl:template match="*[contains(@class,' topic/fig ')]">
    <fo:block xsl:use-attribute-sets="fig">
      <xsl:call-template name="commonattributes"/>
      <xsl:call-template name="setFrame"/>
      <xsl:call-template name="setExpanse"/>
      <xsl:if test="not(@id)">
        <xsl:attribute name="id">
          <xsl:call-template name="get-id"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="*[contains(@class,' topic/title ')]"/>
      <xsl:apply-templates select="*[not(contains(@class,' topic/title ') or contains(@class,' topic/desc '))]"/>
      <xsl:apply-templates select="*[contains(@class,' topic/desc ')]"/>
    </fo:block>
  </xsl:template>
  
  <!-- TODO: make information-mapping-like titled sections -->
  <xsl:template match="*[contains(@class,' topic/section ')]">
    <fo:block xsl:use-attribute-sets="section">
      <xsl:call-template name="commonattributes"/>
      <xsl:apply-templates select="." mode="dita2xslfo:section-heading"/>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
  
  <xsl:template match="*" mode="placeImage">
    <xsl:param name="imageAlign"/>
    <xsl:param name="href"/>
    <xsl:param name="height"/>
    <xsl:param name="width"/>
    <!--Using align attribute set according to image @align attribute-->
    <xsl:call-template name="processAttrSetReflection">
      <xsl:with-param name="attrSet" select="concat('__align__', $imageAlign)"/>
      <xsl:with-param name="path" select="'../../cfg/fo/attrs/commons-attr.xsl'"/>
    </xsl:call-template>
      <fo:external-graphic src="url({$href})" xsl:use-attribute-sets="image">
      <!--Setting image height if defined-->
      <xsl:if test="$height">
        <xsl:attribute name="content-height">
          <xsl:choose>
            <xsl:when test="not(string(number($height)) = 'NaN')">
              <xsl:value-of select="concat($height, 'px')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$height"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </xsl:if>
      <!--Setting image width if defined-->
      <xsl:if test="$width">
        <xsl:attribute name="content-width">
          <xsl:choose>
            <xsl:when test="not(string(number($width)) = 'NaN')">
              <xsl:value-of select="concat($width, 'px')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$width"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="not($width) and not($height) and @scale">
        <xsl:attribute name="content-width">
          <xsl:value-of select="concat(@scale,'%')"/>
        </xsl:attribute>
      </xsl:if>
      <!-- Scaling to fit available width (frabad custom) -->
      <xsl:if test="not($width) and not($height) and not(@scale) and @scalefit='yes'">
        <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
        <xsl:attribute name="width">100%</xsl:attribute>
      </xsl:if>
    </fo:external-graphic>
  </xsl:template>

  <!-- normalize spaces in keyword inline output -->
  <xsl:template match="*[contains(@class,' topic/keyword ')]" name="topic.keyword">
    <xsl:param name="keys" select="@keyref" as="attribute()?"/>
    <xsl:param name="contents" as="node()*">
      <xsl:variable name="target" select="key('id', substring(@href, 2))"/>
      <xsl:choose>
        <xsl:when test="not(normalize-space(.)) and $keys and $target/self::*[contains(@class,' topic/topic ')]">
          <xsl:apply-templates select="$target/*[contains(@class, ' topic/title ')]/node()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:variable name="topicref" select="key('map-id', substring(@href, 2))"/>
    <xsl:choose>
      <xsl:when test="$keys and @href and not($topicref/ancestor-or-self::*[@linking][1]/@linking = ('none', 'sourceonly'))">
        <fo:basic-link xsl:use-attribute-sets="xref keyword">
          <xsl:call-template name="commonattributes"/>
          <xsl:call-template name="buildBasicLinkDestination"/>
          <xsl:value-of select="normalize-space($contents)"/>
        </fo:basic-link>
      </xsl:when>
      <xsl:otherwise>
        <fo:inline xsl:use-attribute-sets="keyword">
          <xsl:call-template name="commonattributes"/>
          <xsl:value-of select="normalize-space($contents)"/>
        </fo:inline>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
