custom PDF output for the DITA Open Toolkit

Changes to the default PDF plugin include the following:

- A4 as the default page dimensions
- Linux Libertine as the default font
- more aggressive body indent
- more space before titles
- increased page-number size
- table-layout headers
- full page-width front cover image using ditamap data
- list-style definitions
- footer-image using ditamap data

